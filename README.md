# Soldering Fan Controller
## Description
Hardware part of project to control two fans to soak soldering fumes.
Both fans can be turned on/off by buttons and their speed it set by potentiometer. Project uses AtMega328p as main CPU, OLED display to show power of fans and their status. 

## Software part
Link to repository with software for project: https://gitlab.com/stengljirka/soldering-fan-controller-sw

## License
Project is under GPLv3 licence. 
